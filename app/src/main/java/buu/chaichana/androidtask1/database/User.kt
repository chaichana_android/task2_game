package buu.chaichana.androidtask1.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_table")
data class User(
    @PrimaryKey(autoGenerate = true)
    var userId: Long = 0L,

    @ColumnInfo(name = "name")
    val userName: String = "",

    @ColumnInfo(name = "correct")
    val userCorrect: Int = 0,

    @ColumnInfo(name = "incorrect")
    val userIncorrect: Int = 0
)