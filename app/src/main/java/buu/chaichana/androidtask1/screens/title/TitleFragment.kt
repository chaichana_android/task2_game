package buu.chaichana.androidtask1.screens.title

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import buu.chaichana.androidtask1.R
import buu.chaichana.androidtask1.databinding.FragmentTitleBinding


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TitleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TitleFragment : Fragment() {
    private lateinit var binding: FragmentTitleBinding
    private lateinit var viewModel: TitleViewModel
    private lateinit var viewModelFactory: TitleViewModelFactory
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentTitleBinding>(
            inflater, R.layout.fragment_title, container, false
        )
        viewModelFactory = TitleViewModelFactory(
            finalCorrect = TitleFragmentArgs.fromBundle(requireArguments()).numCorrect,
            finalIncorrect = TitleFragmentArgs.fromBundle(requireArguments()).numIncorrect
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(TitleViewModel::class.java)

        binding.btnPlus.setOnClickListener { view: View ->
            view.findNavController().navigate(
                TitleFragmentDirections.actionTitleFragmentToGameFragment(
                    viewModel.correct.value!!,
                    viewModel.incorrect.value!!,
                    viewModel.plus.value!!
                )
            )
        }
        binding.btnMinus.setOnClickListener { view: View ->
            view.findNavController().navigate(
                TitleFragmentDirections.actionTitleFragmentToGameFragment(
                    viewModel.correct.value!!,
                    viewModel.incorrect.value!!,
                    viewModel.minus.value!!
                )
            )
        }
        binding.btnMultiple.setOnClickListener { view: View ->
            view.findNavController().navigate(
                TitleFragmentDirections.actionTitleFragmentToGameFragment(
                    viewModel.correct.value!!,
                    viewModel.incorrect.value!!,
                    viewModel.multiple.value!!
                )
            )
        }
        binding.titleViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

}