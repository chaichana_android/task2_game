package buu.chaichana.androidtask1.screens.title

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import buu.chaichana.androidtask1.database.User

class TitleViewModel(finalCorrect: Int, finalIncorrect: Int) : ViewModel() {
    //User
    private val _user = MutableLiveData<User?>()
    val user: LiveData<User?>
        get() = _user
    private val _correct = MutableLiveData<Int>()
    val correct: LiveData<Int>
        get() = _correct
    private val _incorrect = MutableLiveData<Int>()
    val incorrect: LiveData<Int>
        get() = _incorrect
    private val _username = MutableLiveData<String>()
    val username: LiveData<String>
        get() = _username

    private val _plus = MutableLiveData<Int>()
    val plus: LiveData<Int>
        get() = _plus
    private val _minus = MutableLiveData<Int>()
    val minus: LiveData<Int>
        get() = _minus
    private val _multiple = MutableLiveData<Int>()
    val multiple: LiveData<Int>
        get() = _multiple
    init {
        _correct.value = finalCorrect
        _incorrect.value = finalIncorrect
        _plus.value = 0
        _minus.value = 1
        _multiple.value = 2
    }
}