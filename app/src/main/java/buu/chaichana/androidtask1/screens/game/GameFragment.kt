package buu.chaichana.androidtask1.screens.game

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import buu.chaichana.androidtask1.R
import androidx.navigation.fragment.NavHostFragment
import buu.chaichana.androidtask1.database.UserDatabase
import buu.chaichana.androidtask1.databinding.FragmentGameBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [TitleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class GameFragment : Fragment() {
    private lateinit var binding: FragmentGameBinding
    private lateinit var viewModel: GameViewModel
    private lateinit var viewModelFactory: GameViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentGameBinding>(
            inflater,
            R.layout.fragment_game,
            container,
            false
        )
        Log.i("GameFragment", "Called ViewModelProvider.get")
        val application = requireNotNull(this.activity).application
        val dataSource = UserDatabase.getInstance(application).userDatabaseDao
        viewModelFactory = GameViewModelFactory(
            finalFrag = GameFragmentArgs.fromBundle(requireArguments()).numFrag,
            dataSource = dataSource,
            application = application
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(GameViewModel::class.java)
        binding.btnSave.setOnClickListener { gameFin() }
        binding.gameViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }
    private fun gameFin() {
        NavHostFragment.findNavController(this).navigate(
            GameFragmentDirections.actionGameFragmentToTitleFragment(
            viewModel.correct.value!!,
            viewModel.incorrect.value!!
        ))
    }
}