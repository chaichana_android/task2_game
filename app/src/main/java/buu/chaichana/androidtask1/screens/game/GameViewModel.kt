package buu.chaichana.androidtask1.screens.game

import android.app.Application
import android.os.Handler
import android.util.Log
import android.widget.Button
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import buu.chaichana.androidtask1.database.User
import buu.chaichana.androidtask1.database.UserDatabaseDao
import kotlinx.coroutines.launch
import java.util.*

class GameViewModel(finalFrag: Int, private val database: UserDatabaseDao, application: Application
) : AndroidViewModel(application) {

    //User
    private val _user = MutableLiveData<User?>()
    val user: LiveData<User?>
        get() = _user
    private val _correct = MutableLiveData<Int>()
    val correct: LiveData<Int>
        get() = _correct
    private val _incorrect = MutableLiveData<Int>()
    val incorrect: LiveData<Int>
        get() = _incorrect
    private val _username = MutableLiveData<String>()
    val username: LiveData<String>
        get() = _username
    //For run game
    private val _ansNumber = MutableLiveData<Int>()
    val ansNumber: LiveData<Int>
        get() = _ansNumber
    private val _choiceNumber = MutableLiveData<Int>()
    val choiceNumber: LiveData<Int>
        get() = _choiceNumber
    private val _firstRandNumber = MutableLiveData<Int>()
    val firstRandNumber: LiveData<Int>
        get() = _firstRandNumber
    private val _secondRandNumber = MutableLiveData<Int>()
    val secondRandNumber: LiveData<Int>
        get() = _secondRandNumber
    private val _firstChoice = MutableLiveData<Int>()
    val firstChoice: LiveData<Int>
        get() = _firstChoice
    private val _secondChoice = MutableLiveData<Int>()
    val secondChoice: LiveData<Int>
        get() = _secondChoice
    private val _thirdChoice = MutableLiveData<Int>()
    val thirdChoice: LiveData<Int>
        get() = _thirdChoice
    //for alertText
    private val _alertText = MutableLiveData<String>()
    val alertText: LiveData<String>
        get() = _alertText
    private val _thisShow = MutableLiveData<Boolean>()
    val thisShow: LiveData<Boolean>
        get() = _thisShow
    //for check game
    private val _thisCorrect = MutableLiveData<Boolean>()
    val thisCorrect: LiveData<Boolean>
        get() = _thisCorrect
    private val _numFrag = MutableLiveData<Int>()
    val numFrag: LiveData<Int>
        get() = _numFrag
    private val _plusText = MutableLiveData<String>()
    val plusText: LiveData<String>
        get() = _plusText
    private val _minusText = MutableLiveData<String>()
    val minusText: LiveData<String>
        get() = _minusText
    private val _multipleText = MutableLiveData<String>()
    val multipleText: LiveData<String>
        get() = _multipleText

    init {
        initializeUser()
        _numFrag.value = finalFrag
        _ansNumber.value = 0
        _choiceNumber.value = 0
        _firstRandNumber.value = 0
        _secondRandNumber.value = 0
        _firstChoice.value = 0
        _secondChoice.value = 0
        _thirdChoice.value = 0
        _alertText.value = ""
        _plusText.value = "+"
        _minusText.value = "-"
        _multipleText.value = "x"
        _thisShow.value = false
        _thisCorrect.value = false
        startGame()
        Log.i("GameViewModel", "GameViewModel created!")
        Log.i(
            "GameViewModel",
            "Final correctScore is ${user.value!!.userCorrect} and incorrectScore is ${user.value!!.userIncorrect}"
        )
    }

    override fun onCleared() {
        super.onCleared()
        Log.i("GameViewModel", "GameViewModel destroyed!")
    }

    private fun initializeUser() {
        viewModelScope.launch {
            _user.value = getUserFromDatabase()
            _correct.value = user.value!!.userCorrect
            _incorrect.value = user.value!!.userIncorrect
            _username.value = user.value!!.userName
        }
    }

    private suspend fun getUserFromDatabase(): User? {
        var user = database.getUser()
        if (user?.userCorrect == 0 && user?.userIncorrect == 0) {
            user = null
        }
        return user
    }

    fun startNewUser() {
        viewModelScope.launch {
            val newUser = User()
            insert(newUser)
            _user.value = getUserFromDatabase()
            _correct.value = user.value!!.userCorrect
            _incorrect.value = user.value!!.userIncorrect
            _username.value = user.value!!.userName
        }
    }

    private suspend fun insert(user: User) {
        database.insert(user)

    }

    private suspend fun update(user: User) {
        database.update(user)
    }

    private fun startGame() {
        _thisShow.value = false
        _thisCorrect.value = false
        randomNumber()
        pseudoNumber()
        setButton()
    }

    private fun scoreUp() {
        _thisShow.value = true
        _thisCorrect.value = true
        _correct.value = _correct.value?.plus(1)
        _alertText.value = "Correct!"
    }

    private fun scoreDown() {
        _thisShow.value = true
        _thisCorrect.value = false
        _incorrect.value = _incorrect.value?.plus(1)
        _alertText.value = "Incorrect!"
    }

    fun checkGame(text: Button) {
        if (text.text == ansNumber.value.toString()) {
            scoreUp()
            delayForNewGame()
        } else {
            scoreDown()
        }
    }

    private fun delayForNewGame() {
        Handler().postDelayed({
            startGame()
        }, 1000)
    }

    private fun randomNumber() {
        _firstRandNumber.value = Random().nextInt(9) + 1
        _secondRandNumber.value = Random().nextInt(9) + 1
        when (numFrag.value){
            0 -> {
                _ansNumber.value = firstRandNumber.value!! + secondRandNumber.value!!
            }
            1 -> {
                _ansNumber.value = firstRandNumber.value!! - secondRandNumber.value!!
            }
            2 -> {
                _ansNumber.value = firstRandNumber.value!! * secondRandNumber.value!!
            }
        }
    }

    private fun pseudoNumber() {
        _choiceNumber.value = Random().nextInt(9) + 1
    }

    private fun setButton() {
        when (choiceNumber.value) {
            1,4,7,10 -> {
                _firstChoice.value = ansNumber.value
                _secondChoice.value = ansNumber.value?.plus(1)
                _thirdChoice.value = ansNumber.value?.minus(1)
            }
            2,5,8 -> {
                _firstChoice.value = ansNumber.value?.minus(1)
                _secondChoice.value = ansNumber.value
                _thirdChoice.value = ansNumber.value?.plus(1)
            }
            3,6,9 -> {
                _firstChoice.value = ansNumber.value?.plus(1)
                _secondChoice.value = ansNumber.value?.minus(1)
                _thirdChoice.value = ansNumber.value
            }
        }

    }
}