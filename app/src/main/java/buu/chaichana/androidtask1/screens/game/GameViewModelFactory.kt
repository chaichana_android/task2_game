package buu.chaichana.androidtask1.screens.game

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import buu.chaichana.androidtask1.database.UserDatabaseDao

class GameViewModelFactory(private val finalFrag: Int, private val dataSource: UserDatabaseDao,
                           private val application: Application
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(GameViewModel::class.java)) {
            return GameViewModel(finalFrag, dataSource, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}